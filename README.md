Rust implementation of [golang challenge #1](http://golang-challenge.org/go-challenge1).

A simple binary file format for a drum machine is given. The task is to reverse
it and implement a decoder for it.

## Example output

With the challenge came some examples how the lost executable's outputs looked
like. In `fixtures/` are all the example binaries given. And in `src/tests.rs`
are the respective example outputs.

```
Saved with HW Version: 0.808-alpha
Tempo: 120
(0) kick	|x---|x---|x---|x---|
(1) snare	|----|x---|----|x---|
(2) clap	|----|x-x-|----|----|
(3) hh-open	|--x-|--x-|x-x-|--x-|
(4) hh-close	|x---|x---|----|x--x|
(5) cowbell	|----|----|--x-|----|
```

## File format

There is no real specification. But this is my understanding based on looking
at the provided example files and reading the challenge description.

A file starts with a header and is followed by multiple lines. All numbers are
encoded with big endianness.

### Header

Size | encoding | Description
-----|----------|------------
 6   | ASCII    | magic bytes "SPLICE"
 8   | u64      | file size (bytes _after_ this field are counted)
32   | ASCII    | version string (of the editor used to create the file?)
 4   | f32      | tempo

### Line

Size | Encoding | Description
-----|----------|----------------
 1   | u8       | id
 4   | u32      | length of the name field (n_l)
 n   | ASCII    | name
16   | bool[]   | data (byte-wise when to hit: 0 for no, 1 for yes)

## To-do

- [ ] maybe add something (tests!?) that allows for better/easier debugging
	- [ ] find out how to properly debug

