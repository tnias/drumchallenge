extern crate drumchallenge;
extern crate nom;

use drumchallenge::parse;
use nom::IResult;

use std::io::prelude::*;
use std::fs::File;

use std::env;

fn main() {
    if let Some(arg1) = env::args().nth(1) {
        let filepath = arg1;
        let mut buf: Vec<u8> = vec![];
        let mut file = File::open(filepath).expect("file exists");
        let _ = file.read_to_end(&mut buf).expect("file can be read");

        match parse(&buf[..]) {
            IResult::Done(_, pattern) => {
                println!("{}", pattern);
            }
            e => {
                panic!("something went wrong. {:?}", e);
            }
        }
    } else {
        println!("please provide a filename");
    }
}
