#[test]
fn gochallenge_tests() {
    use parser::parse;
    use nom::IResult;

    use std::io::prelude::*;
    use std::fs::File;

    let table: Vec<(&str, &str)> = vec![
        (
            "fixtures/pattern_1.splice",
            "Saved with HW Version: 0.808-alpha
Tempo: 120
(0) kick	|x---|x---|x---|x---|
(1) snare	|----|x---|----|x---|
(2) clap	|----|x-x-|----|----|
(3) hh-open	|--x-|--x-|x-x-|--x-|
(4) hh-close	|x---|x---|----|x--x|
(5) cowbell	|----|----|--x-|----|
"
        ),
        (
            "fixtures/pattern_2.splice",
            "Saved with HW Version: 0.808-alpha
Tempo: 98.4
(0) kick	|x---|----|x---|----|
(1) snare	|----|x---|----|x---|
(3) hh-open	|--x-|--x-|x-x-|--x-|
(5) cowbell	|----|----|x---|----|
"
        ),
        (
            "fixtures/pattern_3.splice",
            "Saved with HW Version: 0.808-alpha
Tempo: 118
(40) kick	|x---|----|x---|----|
(1) clap	|----|x---|----|x---|
(3) hh-open	|--x-|--x-|x-x-|--x-|
(5) low-tom	|----|---x|----|----|
(12) mid-tom	|----|----|x---|----|
(9) hi-tom	|----|----|-x--|----|
",
        ),
        (
            "fixtures/pattern_4.splice",
            "Saved with HW Version: 0.909
Tempo: 240
(0) SubKick	|----|----|----|----|
(1) Kick	|x---|----|x---|----|
(99) Maracas	|x-x-|x-x-|x-x-|x-x-|
(255) Low Conga	|----|x---|----|x---|
",
        ),
        (
            "fixtures/pattern_5.splice",
            "Saved with HW Version: 0.708-alpha
Tempo: 999
(1) Kick	|x---|----|x---|----|
(2) HiHat	|x-x-|x-x-|x-x-|x-x-|
",
        ),
    ];

    for r in table.iter() {
        println!("expected of {}:\n{}", r.0, r.1);

        let mut buf: Vec<u8> = vec![];
        let mut file = File::open(r.0).expect("file exists");
        let _ = file.read_to_end(&mut buf).expect("file can be read");

        match parse(&buf[..]) {
            IResult::Done(_, pattern) => {
                println!("Parsing is done.");
                assert_eq!(r.1, format!("{}", pattern));
            }
            e => {
                panic!("something went wrong. {:?}", e);
            }
        }

        println!("--------------");
    }
}
