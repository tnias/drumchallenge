#[macro_use]
extern crate nom;

mod parser;
mod tests;

pub use parser::parse;

use std::fmt;

#[derive(Debug)]
pub struct Pattern {
    version: String,
    tempo: f32,
    lines: Vec<Line>,
}

#[derive(Debug)]
pub struct Line {
    id: u8,
    name: String,
    data: Vec<u8>,
}

impl fmt::Display for Pattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut lines = String::new();

        for line in &self.lines {
            lines += format!("{}\n", line).as_str();
        }

        write!(f, "Saved with HW Version: {}\nTempo: {}\n{}",
               self.version, self.tempo, lines)
    }
}

impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut fancy = String::new();

        for (i,x) in self.data.iter().enumerate() {
            fancy += format!(
                    "{}{}",
                    if i%4 == 0 { "|" } else {""},
                    match *x { 0 => "-", 1 => "x", _ => "?" }
                ).as_str();
        }

        write!(f, "({}) {}\t{}|", self.id, self.name, fancy)
    }
}
