use nom::*;
use super::{Pattern, Line};

named!(parse_line<&[u8], Line>,
    do_parse!(
        id:      be_u8              >>
        namelen: be_u32             >>
        name:    take_str!(namelen) >>
        data:    take!(16)          >>
        (
            Line {
                id: id,
                name: name.to_string(),
                data: data.to_vec(),
            }
        )
    )
);

/*
 * _Assumption_: All provided data is to be interpreted
 */
named!(parse_pattern_inner<&[u8], Pattern>,
    do_parse!(
        version: take_str!(32)      >>
        tempo:   le_f32             >>
        lines:   many0!(parse_line) >>
        (
            Pattern{
                version: version.trim_matches('\u{0}').to_string(),
                tempo: tempo,
                lines: lines,
            }
        )
    )
);

named!(parse_pattern<&[u8], Pattern>,
    do_parse!(
        // make sure magic bytes are present
        tag!(b"SPLICE")                                       >>
        // read the length field and give that many bytes to `parse_pattern_inner`
        data: map!(length_data!(be_u64), parse_pattern_inner) >>
        (
            match data {
                // ignore all unused bytes at the end
                IResult::Done(_,pattern) => pattern,
                // XXX: what type of input could trigger this?
                //       -> uncommon values in file length?
                // TODO: make it not panic and handle it gracefully
                // TODO: add extra testcase, that triggers this path
                _ => panic!("this should never be reached")
            }
        )
    )
);

/*
 * Wrapper around inner macros.
 */
pub fn parse(i: &[u8]) -> IResult<&[u8], Pattern> {
    parse_pattern(i)
}
